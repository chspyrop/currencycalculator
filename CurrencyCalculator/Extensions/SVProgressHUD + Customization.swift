//
//  SVProgressHUD + Customization.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 05/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
import SVProgressHUD

public extension SVProgressHUD {
    
    public static func setupDesquaredStyle() {
        
        SVProgressHUD.setForegroundColor(UIColor.desquaredOrangeColor)
        SVProgressHUD.setBackgroundColor(UIColor.desquaredBackgroundColor)
        
    }
    
}
