//
//  String + CalculatorDisplay.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 04/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation

extension String {
    
    
    func formatedForCalculatorDispaly() -> String {
        
        let upperBound = String.Index.init(encodedOffset: maximumDigitsToDisplay)

        if self.count > maximumDigitsToDisplay {
             return String(self[...upperBound])
        } else {
            return self
        }
    }
    
    func calculateValueInCurrenncy(currencyRate: Double) -> String {
        
        if self.contains(",") {
            let tmpValue = self.replacingOccurrences(of: ",", with: ".")
            let newValue = Double(tmpValue)! * currencyRate
            if newValue.truncatingRemainder(dividingBy: 1) == 0 {
                return String(Int(newValue)).formatedForCalculatorDispaly()
            } else {
                return String(newValue).replacingOccurrences(of: ".", with: ",").formatedForCalculatorDispaly()
            }
        } else {
            let tmpValue = Double(self)!
            let newValue = Double(tmpValue) * currencyRate
            if newValue.truncatingRemainder(dividingBy: 1) == 0 {
                return String(Int(newValue)).formatedForCalculatorDispaly()
            } else {
                return String(newValue).replacingOccurrences(of: ".", with: ",").formatedForCalculatorDispaly()
            }
        }
        
    }

}
