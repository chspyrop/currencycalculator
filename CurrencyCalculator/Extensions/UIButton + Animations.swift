//
//  UIButton + Animations.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    
    func numberButtonPressedAnimationStyle () {
                
        self.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.50, delay: 0, options: .allowUserInteraction, animations: {
            self.layer.backgroundColor = UIColor.desquaredOrangeColor.cgColor},completion: nil)
        UIView.animate(withDuration: 0.50, delay: 0, options: .allowUserInteraction, animations: {
            self.layer.backgroundColor = UIColor.desquaredGrayColor.cgColor},completion: nil)

    }
    
    func operatorButtonActivate () {
        self.layer.removeAllAnimations()
        
        UIView.animate(withDuration: 0.50, delay: 0, options: .allowUserInteraction, animations: {
            self.layer.backgroundColor = UIColor.desquaredOrangeColorActivateOperator.cgColor},completion: nil)

    }
    
    func operatorButtonDeactivate () {
        self.layer.removeAllAnimations()

        UIView.animate(withDuration: 0.50, delay: 0, options: .allowUserInteraction, animations: {
            self.layer.backgroundColor = UIColor.desquaredOrangeColor.cgColor},completion: nil)
        
    }


}
