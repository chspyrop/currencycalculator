//
//  UIColor + DesquaredColors.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 28/02/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static let desquaredFontColor = UIColor(red: 232.0/255.0, green: 89.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    static let desquaredBackgroundColor = UIColor(red: 31.0/255.0, green: 47.0/255.0, blue: 63.0/255.0, alpha: 1.0)
    
    static let desquaredOrangeColor = UIColor(red: 232.0/255.0, green: 89.0/255.0, blue: 74.0/255.0, alpha: 1.0)
    static let desquaredOrangeColorActivateOperator = UIColor(red: 232.0/255.0, green: 89.0/255.0, blue: 74.0/255.0, alpha: 0.5)
    static let desquaredGrayColor = UIColor(red: 169.0/255.0, green: 169.0/255.0, blue: 169.0/255.0, alpha: 1.0)

}
