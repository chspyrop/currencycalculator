//
//  Preferences.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 04/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
struct Preferences {
    static let defaults = UserDefaults.standard
    
    static var lastBasedCurrency: String {
        get { return self.defaults.string(forKey: #function) ?? "EUR" }
        set { self.defaults.set(newValue, forKey: #function) }
        
    }

    static var lastCalculatedCurrency: String {
        get { return self.defaults.string(forKey: #function) ?? "USD" }
        set { self.defaults.set(newValue, forKey: #function) }
        
    }

}
