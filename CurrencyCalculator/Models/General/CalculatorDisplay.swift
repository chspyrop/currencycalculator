//
//  CalculatorDisplay.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation

struct CalculatorDisplay: Codable {
    var value: String
    var currencySymbol: String
}
