//
//  CurrencyObject.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 03/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
import RealmSwift

final class CurrencyObject: Object {
    
    @objc dynamic var baseCurrency: String? = nil
    @objc dynamic var lastUpdate: String? = nil
    let rates = List<RateObject>()
    
    override static func primaryKey() -> String? {
        return "baseCurrency"
    }
}
