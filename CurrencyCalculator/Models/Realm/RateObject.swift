//
//  RateObject.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 03/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
import RealmSwift

final class RateObject: Object {
    
    @objc dynamic var rateID: String? = nil
    @objc dynamic var baseCurrency: String? = nil
    @objc dynamic var currency: String? = nil
    var rate = RealmOptional<Double>()

    override static func primaryKey() -> String {
        return "rateID"
    }
    
}
