//
//  RatesForBaseResponse.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 03/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation

struct RatesForBaseResponse : Decodable {
    let rates: [String:Double?]
    let base: String
    let date: String
}
