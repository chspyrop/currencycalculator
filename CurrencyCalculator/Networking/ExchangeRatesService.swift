//
//  ExchangeRatesService.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 03/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import Foundation
import Moya

enum ExchangeRatesService {
    case getRatesBasedOn(currency: String)
}

extension ExchangeRatesService: TargetType {
    var baseURL: URL {
        return URL(string: "https://api.openrates.io")!
    }
    
    var path: String {
        switch self {
        case .getRatesBasedOn:
            return "/latest"
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getRatesBasedOn:
            return .get
        }
    }
    
    var sampleData: Data {
        //For testing
        return "{}".data(using: .utf8)!
    }
    
    var task: Task {
        switch self {
        case .getRatesBasedOn(let currency):
            return .requestParameters(parameters: ["base":currency], encoding: URLEncoding.queryString)
        }
    }
    
    var headers: [String : String]? {
        return ["Content-Type": "application/json"]
    }
    
}
