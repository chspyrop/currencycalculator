//
//  CalculatorActions.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import ReSwift
import Moya
import RealmSwift

let exchangeRatesService = MoyaProvider<ExchangeRatesService>()//(plugins: [NetworkLoggerPlugin(verbose: true)])

let backgroundQueue = DispatchQueue(label: "com.currencyCalculator.queue",
                                    qos: .background,
                                    target: nil)

public enum calculatorAction: Action {
    
    case numberPressed(number: Int)
    case operatorPressed(theOperator: operatorType)
    case RefreshDisplay
    case error
    
}

public enum operatorType: Int {
    case comma = 10
    case delete = 20
    case ac = 30
    
    case emptyOperator = 40
    case division = 41
    case multiplication = 42
    case substaction = 43
    case addition = 44
    case equality = 45
}

func getExchangeRatesForCurrency(currency: String) -> (CalculatorState, Store<CalculatorState>) -> Action? {
    return { state, store in
        exchangeRatesService.request(.getRatesBasedOn(currency: currency)) { (result) in
            switch result {
            case .success(let response):
                do {
                    let decoder = JSONDecoder()
                    let ratesForBaseResponse = try decoder.decode(RatesForBaseResponse.self, from: response.data)
                                        
                    backgroundQueue.async {
                        let realm = try! Realm()

                        try! realm.write {//Add or Update rates for base currency
                            for rate in ratesForBaseResponse.rates {
                                let rateObject = RateObject()
                                rateObject.rateID = ratesForBaseResponse.base + rate.key
                                rateObject.baseCurrency = ratesForBaseResponse.base
                                rateObject.currency = rate.key
                                rateObject.rate.value = rate.value

                                realm.add(rateObject, update: true)
                            }
                        }
                        
                        let predicate = NSPredicate(format: "baseCurrency == '\(ratesForBaseResponse.base)'")
                        let ratesObjects = realm.objects(RateObject.self).filter(predicate)

                        let currencyObject = CurrencyObject()
                        currencyObject.baseCurrency = ratesForBaseResponse.base
                        currencyObject.lastUpdate = ratesForBaseResponse.date
                        for rateObject in ratesObjects {
                            currencyObject.rates.append(rateObject)
                        }
                        
                        try! realm.write {//Add or Update currency with rates
                            realm.add(currencyObject, update: true)
                        }
                                                
                    }
                } catch {
                    print("Error")
                }
            case .failure(let error):
                print(error)
            }
        }
        return calculatorAction.error
    }
}
