//
//  CalculatorReducer.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import ReSwift

let maximumDigitsToDisplay = 7

fileprivate func resetPendigOperations(_ state: inout CalculatorState) {
    state.pendingOperand = "0"
    state.pendingOperator = operatorType.emptyOperator
    state.removeOldOperand = true
    state.pendingOperatorStatus = false
}

fileprivate func cutDecimalsIfNeeded(number: Double) -> String {
    
    let upperBound = String.Index.init(encodedOffset: maximumDigitsToDisplay)

    if number.truncatingRemainder(dividingBy: 1) == 0 {
        //it's an integer
        return String(Int(number))
    } else {
        let numberToReturnFormated = String(Double(number)).replacingOccurrences(of: ".", with: ",")
        if numberToReturnFormated.count > maximumDigitsToDisplay {
            return String(numberToReturnFormated[...upperBound])
        } else {
             return numberToReturnFormated
        }
    }
}

fileprivate func resolvePendingOperationsIfNeeded (_ state: inout CalculatorState) {
    
    let pendingOperand = state.pendingOperand.replacingOccurrences(of: ",", with: ".")
    let calculatorDisplay = state.calculatorDisplay.replacingOccurrences(of: ",", with: ".")
    
    
    switch state.pendingOperator {
        
    case operatorType.division:
        state.calculatorDisplay =  String(cutDecimalsIfNeeded(number:Double(pendingOperand)! / Double(calculatorDisplay)!))
        resetPendigOperations(&state)
    case operatorType.multiplication:
        state.calculatorDisplay =  String(cutDecimalsIfNeeded(number:Double(pendingOperand)! * Double(calculatorDisplay)!))
        resetPendigOperations(&state)
    case operatorType.substaction:
        state.calculatorDisplay =  String(cutDecimalsIfNeeded(number:Double(pendingOperand)! - Double(calculatorDisplay)!))
        resetPendigOperations(&state)
    case operatorType.addition:
        state.calculatorDisplay =  String(cutDecimalsIfNeeded(number:Double(pendingOperand)! + Double(calculatorDisplay)!))
        resetPendigOperations(&state)
    default:///peratorType
        break
    }

}

func calculatorReducer(action: Action, state: CalculatorState?) -> CalculatorState {
    
    // if no state has been provided, create the default state
    var state = state ?? CalculatorState()
    state.refreshDisplay = false
    
    switch action {
        
    case calculatorAction.RefreshDisplay:
        state.refreshDisplay = true
        
    case calculatorAction.numberPressed(let number):
        
        if state.removeOldOperand == true {//Number after operator
            state.calculatorDisplay = "0"
            state.removeOldOperand = false
        }
        
        if state.calculatorDisplay == "0" {
            state.calculatorDisplay = String(number)
        } else {
            if state.calculatorDisplay.count <= maximumDigitsToDisplay {
                state.calculatorDisplay = state.calculatorDisplay + String(number)
            } 
        }
        
    case calculatorAction.operatorPressed(let theOperator):
                
        switch theOperator {
            
        case operatorType.ac:
            
            state.calculatorDisplay = "0"
            state.pendingOperand = "0"
            state.pendingOperator = operatorType.emptyOperator
            state.pendingOperatorStatus = false
            
        case operatorType.delete:
            
            if state.calculatorDisplay.count > 1 {
                state.calculatorDisplay.removeLast()
            } else {
                state.calculatorDisplay = "0"
            }
          
        case operatorType.comma:
            
            state.removeOldOperand = false

            if state.calculatorDisplay == "0" {
                state.calculatorDisplay = "0,"
            } else {
                if !state.calculatorDisplay.contains(",") {
                    if state.calculatorDisplay.count <= maximumDigitsToDisplay {
                        state.calculatorDisplay = state.calculatorDisplay + ","
                    }
                }
            }
            
        case operatorType.equality:
            
            resolvePendingOperationsIfNeeded(&state)
            
        default://theOperator
            
            resolvePendingOperationsIfNeeded(&state)
            
            if !state.pendingOperatorStatus {
                state.pendingOperand = state.calculatorDisplay
                state.pendingOperator = theOperator
                state.pendingOperatorStatus = true
                state.removeOldOperand = true
            }

        }
            
    default://Action
        break
    }
    
    return state
}
