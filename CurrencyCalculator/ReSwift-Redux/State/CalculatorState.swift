//
//  CalculatorState.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import ReSwift

struct CalculatorState: StateType {
    
    var calculatorDisplay: String = "0"
    var pendingOperatorStatus: Bool = false
    var pendingOperator: operatorType = operatorType.emptyOperator
    var pendingOperand: String = "0"
    var removeOldOperand: Bool = false
    var refreshDisplay: Bool = false
}
