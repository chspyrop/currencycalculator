//
//  CurrencyCalculatorTableViewCell.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 01/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import UIKit

class CurrencyCalculatorTableViewCell: UITableViewCell {

    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var currencySymbolButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with display: CalculatorDisplay) {
        valueLabel.text = display.value
        currencySymbolButton.setTitle(display.currencySymbol, for: .normal)
    }

}
