//
//  CurrencyCalculatorViewController.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 28/02/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import UIKit
import ReSwift
import RealmSwift

class CurrencyCalculatorViewController: UIViewController, StoreSubscriber {

    let spacesBetweenHorizontalButtons: CGFloat = 3.0
    let numberOfButtonsHorizontaly: CGFloat = 4.0
    
    @IBOutlet weak var baseKeyWidthConstrain: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!

    var displays: [CalculatorDisplay] = []
    var pendigOperator: operatorType = operatorType.emptyOperator
    
    var baseCurrency : Results<CurrencyObject>!
    var calculatedCurrency : RateObject? = nil
    var baseCurrencyNotificationToken: NotificationToken?

    var currencyIndexForSelection: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.rowHeight = baseKeyWidthConstrain.constant
        
        displays.append(CalculatorDisplay(value: "0", currencySymbol: Preferences.lastBasedCurrency))
        displays.append(CalculatorDisplay(value: "0", currencySymbol: Preferences.lastCalculatedCurrency))
        
        calculatorStore.dispatch(getExchangeRatesForCurrency(currency: Preferences.lastBasedCurrency))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        baseKeyWidthConstrain.constant = ((UIScreen.main.bounds.width - spacesBetweenHorizontalButtons) / numberOfButtonsHorizontaly)
        calculatorStore.subscribe(self)
        
        RealmManager.shared.observeRealmErrors(in: self) { (error) in
            print(error ?? "no error detected")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        calculatorStore.unsubscribe(self)
        
        baseCurrencyNotificationToken?.invalidate()
        RealmManager.shared.stopObservingErrors(in: self)

    }

    
    override func viewDidAppear(_ animated: Bool) {
        initWithLastUsedCurrencies()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCurrencySelectionSeque"
        {
            if let destinationVC = segue.destination as? CurrencySelectionViewController {
                destinationVC.delegate = self
                
                destinationVC.currencySymbolForSelection = Preferences.lastBasedCurrency

            }
        }
    }

    // MARK: Private
    
    fileprivate func initWithLastUsedCurrencies() {
        
        baseCurrency = RealmManager.shared.realm.objects(CurrencyObject.self).filter("baseCurrency == '\(Preferences.lastBasedCurrency)'")
        
        //Observing baseCurrency modifications in order to update calculatedCurrency when data are available
        
        baseCurrencyNotificationToken = baseCurrency.observe { change in
            switch change {
                
            case .initial, .update:
                self.calculatedCurrency = RealmManager.shared.realm.objects(RateObject.self).filter("baseCurrency == '\(Preferences.lastBasedCurrency)' AND currency == '\(Preferences.lastCalculatedCurrency)'").first
                calculatorStore.dispatch(calculatorAction.RefreshDisplay)
            case .error(let error):
                fatalError("\(error)")
            }
        }

    }
    
    // MARK: IBActions
    
    @IBAction func fetchExchangeRates(_ sender: UIButton) {
        
        calculatorStore.dispatch(getExchangeRatesForCurrency(currency: Preferences.lastBasedCurrency))
        
    }
    
    @IBAction func numberButtonPressed(_ sender: UIButton) {
        
        sender.numberButtonPressedAnimationStyle()
        calculatorStore.dispatch(calculatorAction.numberPressed(number: sender.tag))
        
    }
    
    @IBAction func operatorButtonPressed(_ sender: UIButton) {
        
        calculatorStore.dispatch(calculatorAction.operatorPressed(theOperator: operatorType(rawValue: sender.tag)!))
        
    }

    @IBAction func currencySymbolButtonPressed(_ sender: UIButton) {
        let cell = sender.superview?.superview as! CurrencyCalculatorTableViewCell
        let indexPath = tableView.indexPath(for: cell)
        self.currencyIndexForSelection = indexPath!.row
        performSegue(withIdentifier: "showCurrencySelectionSeque", sender: nil)

    }
    
    
    // MARK: StoreSubscriber
    
    func newState(state: CalculatorState) {
        
        //Refresh calculator display after modification of currency (BaseCurrency or CalculatedCurrency)
        if state.refreshDisplay {
            
            if displays.count == 2 {
                displays[0].currencySymbol = Preferences.lastBasedCurrency
                displays[1].currencySymbol = Preferences.lastCalculatedCurrency

                displays[0].value = state.calculatorDisplay
                if calculatedCurrency != nil {
                    if let rate = calculatedCurrency?.rate.value {
                        displays[1].value = state.calculatorDisplay.calculateValueInCurrenncy(currencyRate: rate)
                        
                    }
                }
                self.tableView.reloadData()
                return
            }
        }
        
        //Update calculator display if needed
        if displays.count == 2 {
            if displays[0].value == state.calculatorDisplay { //Value not changed
                print("No change for the calculatorDisplay")
            } else { //Value changed
                displays[0].value = state.calculatorDisplay
                if calculatedCurrency != nil {
                    if let rate = calculatedCurrency?.rate.value {
                        displays[1].value = state.calculatorDisplay.calculateValueInCurrenncy(currencyRate: rate)

                    }
                }
                self.tableView.reloadData()
                print(state.calculatorDisplay)
            }
        }
        
        //Activation & Deactivation operators buttons if beeded
        if state.pendingOperatorStatus == true {//Diactivate when number types after operator
            if let tmpButton = self.view.viewWithTag(self.pendigOperator.rawValue) as? UIButton {
                tmpButton.operatorButtonDeactivate()
            }
        }
        
        if state.pendingOperator != self.pendigOperator {//Deactivate & Activate if needed when change operator
            if self.pendigOperator != operatorType.emptyOperator {
                if let tmpButton = self.view.viewWithTag(self.pendigOperator.rawValue) as? UIButton {
                    tmpButton.operatorButtonDeactivate()
                }
            }
            self.pendigOperator = state.pendingOperator
            
            if self.pendigOperator != operatorType.emptyOperator {
                if let tmpButton = self.view.viewWithTag(self.pendigOperator.rawValue) as? UIButton {
                    tmpButton.operatorButtonActivate()
                }
            }
        }
        
    }//newState


}//CurrencyCalculatorViewController


extension CurrencyCalculatorViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return displays.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CurrencyCalculatorTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "calculatorCell") as! CurrencyCalculatorTableViewCell
        
        let display = displays[indexPath.row] as CalculatorDisplay
        
        cell.configure(with: display)
        cell.valueLabel.accessibilityIdentifier = "label_result_\(indexPath.row)"

        return cell
    }
    
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return baseKeyWidthConstrain.constant
        
    }
    
}

extension CurrencyCalculatorViewController: CurrencySelectionViewControllerDelegate {
    func didSelectCurrency(currencySymbol: String) {
        
        switch currencyIndexForSelection {
        case 0: //baseCurrency
            Preferences.lastBasedCurrency = currencySymbol
        case 1: //calculatedCurrency
            Preferences.lastCalculatedCurrency = currencySymbol
        default:
            return
        }
        
        self.initWithLastUsedCurrencies()

    }
    
}

