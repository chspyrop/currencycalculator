//
//  CurrencySelectionTableViewCell.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 05/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import UIKit

class CurrencySelectionTableViewCell: UITableViewCell {

    @IBOutlet weak var currencySymbolLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(with rate: RateObject) {
        currencySymbolLabel.text = rate.currency
    }

}
