//
//  CurrencySelectionViewController.swift
//  CurrencyCalculator
//
//  Created by CHARALAMPOS SPYROPOULOS on 05/03/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import UIKit
import RealmSwift

protocol CurrencySelectionViewControllerDelegate {
    func didSelectCurrency(currencySymbol: String )
}

class CurrencySelectionViewController: UIViewController {

    var delegate : CurrencySelectionViewControllerDelegate?
    var currencySymbolForSelection: String!
    var currenciesRates: Results<RateObject>!
    var notificationToken: NotificationToken?

    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.delegate = self
        calculatorStore.dispatch(getExchangeRatesForCurrency(currency: currencySymbolForSelection))
        fetchLocalDataForRates(currency: currencySymbolForSelection)
        
    }
    
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Private
    
    func fetchLocalDataForRates(currency: String) {
        let realm = RealmManager.shared.realm
        let predicate = NSPredicate(format: "baseCurrency == '\(currencySymbolForSelection!)'")
        currenciesRates = realm.objects(RateObject.self).filter(predicate).sorted(byKeyPath: "currency" ,ascending: true)

        notificationToken = currenciesRates.observe { change in
            guard let tableView = self.tableView else { return }
            switch change {
            case .initial:
                tableView.reloadData()
                tableView.layoutIfNeeded()
                tableView.setContentOffset(.zero, animated: true)
                
            case .update(_, let deletions, let insertions, let modifications):
                tableView.insertRows(at: insertions.map({ IndexPath(row: $0, section: 0) }),
                                     with: .automatic)
            case .error(let error):
                fatalError("\(error)")
            }
            
        }

    }

}

extension CurrencySelectionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currenciesRates.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: CurrencySelectionTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "currencyCell") as! CurrencySelectionTableViewCell
        
        let currencyRate = currenciesRates[indexPath.row] as RateObject
        cell.configure(with: currencyRate)
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return 90
        
    }

}

extension CurrencySelectionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if delegate != nil {
            let selectedCurrency = currenciesRates[(self.tableView.indexPathForSelectedRow?.row)!]
            calculatorStore.dispatch(getExchangeRatesForCurrency(currency: selectedCurrency.currency!))
            delegate?.didSelectCurrency(currencySymbol: selectedCurrency.currency!)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
