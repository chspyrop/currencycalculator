//
//  CurrencyCalculatorUITests.swift
//  CurrencyCalculatorUITests
//
//  Created by CHARALAMPOS SPYROPOULOS on 28/02/2019.
//  Copyright © 2019 HomeProduction. All rights reserved.
//

import XCTest

class CurrencyCalculatorUITests: XCTestCase {

    let app = XCUIApplication()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testAddition() {

        app.buttons["AC"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["+"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "12")
    }

    func testSubtraction() {
        
        app.buttons["AC"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["-"].tap()
        app.buttons["5"].tap()
        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "5")
    }

    func testMultiplication() {
        
        app.buttons["AC"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["x"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "20")
    }

    func testDivision() {
        
        app.buttons["AC"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["÷"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "5")
    }

    func testMultipleCalculations() {
        
        app.buttons["AC"].tap()
        
        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["+"].tap()
        app.buttons["2"].tap()
        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "12")
        
        app.buttons["x"].tap()
        app.buttons["3"].tap()
        app.buttons["5"].tap()

        app.buttons["+"].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "420")

        app.buttons["8"].tap()
        app.buttons["8"].tap()

        app.buttons["x"].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "508")
        
        app.buttons["8"].tap()
        app.buttons["="].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "4064")

    }

    func testCalculationsWithDecimalPlaces() {
        
        app.buttons["AC"].tap()
        
        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons[","].tap()
        app.buttons["0"].tap()

        app.buttons["+"].tap()
        app.buttons["2"].tap()
        app.buttons[","].tap()
        app.buttons["2"].tap()

        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "12,2")
        
        app.buttons["AC"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons[","].tap()
        app.buttons["0"].tap()

        app.buttons["+"].tap()
        app.buttons["2"].tap()
        app.buttons[","].tap()
        app.buttons["0"].tap()

        app.buttons["="].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "12")

        app.buttons["AC"].tap()

        app.buttons["5"].tap()
        app.buttons["÷"].tap()
        app.buttons["2"].tap()

        app.buttons["="].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "2,5")

    }
    
    func testDelete() {
        app.buttons["AC"].tap()
        
        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons[","].tap()
        app.buttons["1"].tap()

        app.buttons["Delete"].tap()
        app.buttons["Delete"].tap()

        app.buttons["+"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()

        app.buttons["="].tap()
        
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "20")

    }
    
    func testNegativeNumbers() {
        app.buttons["AC"].tap()

        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["-"].tap()
        app.buttons["2"].tap()
        app.buttons["0"].tap()

        app.buttons["="].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "-10")
        
        app.buttons["-"].tap()
        app.buttons["1"].tap()
        app.buttons["0"].tap()
        app.buttons["0"].tap()

        app.buttons["+"].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "-110")

        app.buttons["="].tap()

        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: "label_result_0").label, "-220")

    }
}
