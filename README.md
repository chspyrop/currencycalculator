# Currency calculator

Simple (not scientific) calculator app with the ability to convert the amount between different currencies using a public API.

![picture](Screenshots/CurrencyCalculatorViewController.png =375x812) ![picture](Screenshots/CurrencySelectionViewController.png =375x812)


### Installing

1. Clone the repo
2. Make sure that Cocoapods is installed ($ sudo gem install cocoapods)
3. Move to the project directory, you can do this with $ cd path/to/project/
4. Run the command $ pod install
5. Open the CurrencyCalculator.xcworkspace with XCode

### Dependencies

- Alamofire/Moya
- Realm
- ReSwift
- SDWebImage (for possible improvement to include country flag image for each currency)
- SVProgressHUD (for possible improvement, indication when fetching exchange rates)

### Public API

- [OpenRates.io](https://openrates.io) API for exchange rate. All currency data is sourced from the European Central Bank 

### Good decisions

- Dynamic Autolayout for Calculator UI: All the layouts constraints are based on dynamic calculation of one button (bottom left) depending on device width in order to have all the buttons squared on every possible device (iPhone, not Universal at the moment)
- Redux architecture for basic calculator functionality (ReSwift library)
- Moya library for API calls
- Realm database for storage
- Extensions for UIColor, UIButton, String in order to support theme colors and animations
- Preferences (UseDefaults) in order to support default currencys and last selected currencys
- Delegates protocol to handle Currency selection changes
- Decodable protocol for JSON
- Local storage incrementally build with exchange rates while user makes a new currency selection
- UI Tests for basic caclulator functionality

### Bad decisions

- Currently the Display of the Calculator managed by CalculatorViewController.

### Possible improvements (more time needed)

- Handle internet connection errors with message alert
- Currency conversion also with Redux. (Redux architecture currently supports the calculator functionality. The Currency conversion logic happens on CalculatorViewController)
- Separation in different viewController the Display of the Calculator and embed in container. (Currently the Display of the Calculator managed by CalculatorViewController)
- Update the "Rates" button with the last date of fetched exchange rates for the current selection

### Author

* [Babis Spyropoulos](https://www.linkedin.com/in/chspyrop/)
